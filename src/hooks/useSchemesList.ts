import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getSchemesList} from "../__data__/actions/get-schemes";

export const useSchemesList = () => {
    const state = useSelector((state) => ({
        data: state['tech-schemes'].tsViewing.data,
        isLoading: state['tech-schemes'].tsViewing.loading
    }));
    const dispatch = useDispatch();

    useEffect(() => {
        if (state.data.length === 0 && !state.isLoading) {
            dispatch(getSchemesList());
        }
    }, []);

    return state;
}
