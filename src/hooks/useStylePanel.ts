import {makeStyles} from "@material-ui/core/styles";

export const useStylePanel = makeStyles({
    root: {
        width: "100%",
        height: "100%",
    },
    tree: {
        width: "100%",
        height: "100%",
        padding: "10px"
    },
    box: {
        border: "2px solid rgb(0,0,238)",
        height: "100%;",
        display: "grid",
        gridTemplateRows: "49px auto"
    },
    header: {
        background: "rgb(0,0,238);color:white;padding:10px;",
        margin: 0
    },
    content: {
        color: "#333",
        padding: "10px",
        resize: "none",
        width: "100%",
        height: "100%"
    }
});
