import React from "react";
import {describe, it, expect, beforeEach} from '@jest/globals'
import {mount} from 'enzyme'
import axios from 'axios';
import {Provider} from "react-redux";
import MockAdapter from "axios-mock-adapter";
import { store } from "../../../__data__/store";
import {NotFound} from "../../not-found";

describe('тестируем view NotFound', () => {
    let mockApi;
    beforeEach(() => {
        mockApi = new MockAdapter(axios);
        console.log(mockApi);
    });
    it('тестируем рендер компонента NotFound', async () => {
        const Component = mount(
                <Provider store={store}>
                    <NotFound/>
                </Provider>
        );
        expect(Component).toMatchSnapshot();        
    });
});
