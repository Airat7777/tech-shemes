import React from 'react';
import styles from './styles.css'


class NotFound extends React.Component<any, any> {

    render() {
        return (
            <header className={styles.wrapper}>
                <h1>NotFound 404</h1>
            </header>
        )
    }
}

export { NotFound }
