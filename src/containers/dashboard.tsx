import React from 'react';

import {
    Switch,
    Route,
    Redirect
} from "react-router-dom"

import { URLs } from '../__data__/urls';

import { NotFound } from "./not-found";
import { LazyComponent }  from "../components/index"
import TsViewing from "./ts-viewing";
import TsCatalog from "./ts-catalog";
import TsEditor from "./ts-editor";



const Dashboard = () => (
    <Switch>
        <Route exact path="/">
            <Redirect to={URLs.viewing.url} />
        </Route>
        <Route path={URLs.viewing.url}>
            <LazyComponent>
                <TsViewing/>
            </LazyComponent>
        </Route>
        <Route path={URLs.catalog.url}>
            <LazyComponent>
                <TsCatalog/>
            </LazyComponent>
        </Route>
        <Route path={URLs.editor.url}>
            <LazyComponent>
               <TsEditor/>
            </LazyComponent>
        </Route>
        <Route path="*">
            <LazyComponent>
                <NotFound/>
            </LazyComponent>
        </Route>
    </Switch>
)

export default Dashboard;
