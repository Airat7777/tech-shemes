import React from 'react';
import styles from './styles.css'
import cn from 'classnames';

import {URLs} from "../../__data__/urls";
import {Link} from 'react-router-dom';
import {AppBar, Button, Grid, Toolbar} from "@material-ui/core";
import {withTranslation} from 'react-i18next';

const isActiveClass = (url) => {
    return (window.location.pathname.endsWith(url)) ? styles.activeLink : '';
}

class NavBarPure extends React.Component<any, any> {

    render() {
        const {t} = this.props;
        return (
            <AppBar position="static">
                <Toolbar>
                    <Grid container justify="flex-end">
                        <Link to={URLs.viewing.url} className={cn(styles.linkItem, isActiveClass(URLs.viewing.url))}>
                            <Button color="inherit">{t('tech.schemes.nav.bar.link.view')}</Button>
                        </Link>
                        <Link  to={URLs.catalog.url} className={cn(styles.linkItem, isActiveClass(URLs.catalog.url))}>
                            <Button color="inherit">{t('tech.schemes.nav.bar.link.schemes')}</Button>
                        </Link>
                        <Link to={URLs.editor.url} className={cn(styles.linkItem, isActiveClass(URLs.editor.url))}>
                            <Button color="inherit">{t('tech.schemes.nav.bar.link.add.id')}</Button>
                        </Link>
                    </Grid>
                </Toolbar>
            </AppBar>
        )
    }
}

const NavBar = withTranslation()(NavBarPure);

export {NavBar};
