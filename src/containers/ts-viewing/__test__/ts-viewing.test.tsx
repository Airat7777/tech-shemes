import React from "react";
import {describe, it, expect, beforeEach} from '@jest/globals'
import {mount} from 'enzyme'
import axios from 'axios';
import {Provider} from "react-redux";
import MockAdapter from "axios-mock-adapter";
import { store } from "../../../__data__/store";
import TsViewing from "../index";
import {multipleRequest} from '../../../test-utils';

const schemesListResponse = require('../../../../stubs/api/mocks/schemes-list/get-schemes-list.success');

describe('тестируем view TsViewing', () => {
    let mockApi;
    beforeEach(() => {
        mockApi = new MockAdapter(axios);
    });
    // afterEach(() => {
    //     mockApi.reset();
    // });
    it('тестируем рендер компонента TsViewing', async () => {
        const Component = mount(
                <Provider store={store}>
                    <TsViewing/>
                </Provider>
        );
        expect(Component).toMatchSnapshot();
        const responces = [
            ['GET', '/schemes-list/get-schemes-list', {}, 200, {...schemesListResponse}]
        ];
        await multipleRequest(mockApi, responces);
        Component.update()
        expect(Component.find('div#customId')).toMatchSnapshot();
    });
});
