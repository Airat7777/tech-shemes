import React, {useEffect, useRef} from 'react';
import MetaTags from "react-meta-tags";
import styles from './styles.css'
import {NavBar} from "../nav-bar";
import {useSchemesList} from "../../hooks/useSchemesList";
import {SchemesTree} from  "../../components/schemes-tree";
import SchemeViewer from "../../components/scheme-viewer/scheme-viewer";
import {LogViewer} from "../../components/log-viewer";

import {currScheme} from "../ts-editor";

const TsViewing = () => {
    const state = useSchemesList();
    const ref = useRef(null);

    useEffect(() => {
        state.scheme = currScheme(1);
    }, []);

    if (state.isLoading) {
        return (
            <div>
                loading...
            </div>
        );
    }
    // console.log({state});

    const callback = (schemeIds) => {
        ref.current.setSchemeFromParent(currScheme(schemeIds))
    }

    return (
        <div className={styles.wrapper}>
            <MetaTags>
                <title>Просмотр схемы</title>
            </MetaTags>
            <NavBar/>
            {/*<SvgSave/>*/}
            <main className={styles.main}>
                <div className={`${styles.fill} ${styles.leftPane}`}>
                    <div className={styles.fill}>
                        <SchemesTree state={state} schemSelected={callback}/>
                    </div>
                    <div className={styles.fill}>
                        <LogViewer/>
                    </div>
                </div>
                <div className={styles.fill}>
                    <SchemeViewer ref={ref} imgSrc={currScheme(1)}/>
                </div>
            </main>
        </div>
    )
}
//{JSON.stringify(state.data)}
export default TsViewing
