import React, {useEffect, useRef} from 'react';
import MetaTags from "react-meta-tags";
import styles from './styles.css'
import {NavBar} from "../nav-bar";
import {useSchemesList} from "../../hooks/useSchemesList";
import {SchemesTreeEditor} from "../../components/schemes-tree-editor";
import SchemeViewer from "../../components/scheme-viewer/scheme-viewer";

import TempScheme1 from '../../assets/scheme.svg';
import TempScheme2 from '../../assets/scheme2.svg';
import TempScheme3 from '../../assets/scheme3.svg';
import TempScheme4 from '../../assets/scheme4.svg';

export const currScheme = (num) => {
    switch(num) {
        case "1":
        case "5": { return TempScheme1; break;}
        case "6":
        case "2": { return TempScheme2; break;}
        case "7":
        case "3": { return TempScheme3; break;}
        case "8":
        case "4": { return TempScheme4; break;}
    }
    return TempScheme1;
}

const TsEditor = () => {
    const state = useSchemesList();
    const ref = useRef(null);

    useEffect(() => {
        state.scheme = currScheme(1);
    }, []);

    if (state.isLoading) {
        return (
            <div>
                loading...
            </div>
        );
    }
    // console.log({state});

    const callback = (schemeIds) => {
        ref.current.setSchemeFromParent(currScheme(schemeIds))
    }

    return (
        <div className={styles.wrapper}>
            <MetaTags>
                <title>Редактирование схемы</title>
            </MetaTags>
            
            <NavBar/>
            <main className={styles.main}>
                <div className={`${styles.fill} ${styles.leftPane}`}>
                    <div className={styles.fill}>
                        <SchemesTreeEditor state={state} schemSelected={callback}/>
                    </div>
                </div>
                <div className={styles.fill}>
                    <SchemeViewer ref={ref} imgSrc={currScheme(1)}/>
                </div>
            </main>
        </div>
    )
}

export default TsEditor;
