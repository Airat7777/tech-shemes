import React from "react";
import {describe, it, expect, beforeEach} from '@jest/globals'
import {mount} from 'enzyme'
import axios from 'axios';
import {Provider} from "react-redux";
import MockAdapter from "axios-mock-adapter";
import { store } from "../../../__data__/store";
import TsCatalog from "../";
import {multipleRequest} from '../../../test-utils';

const schemesListResponse = require('../../../../stubs/api/mocks/schemes-list/get-schemes-list.success-lite');

describe('тестируем view TsCatalog', () => {
    let mockApi;
    beforeEach(() => {
        mockApi = new MockAdapter(axios);
    });
    it('тестируем рендер компонента TsCatalog', async () => {
        
        const Component = mount(
                <Provider store={store}>
                    <TsCatalog/>
                </Provider>
        );
        expect(Component).toMatchSnapshot();

        const responces = [
            ['GET', '/schemes-list/get-schemes-list', {}, 200, {...schemesListResponse}]
        ];
        await multipleRequest(mockApi, responces);
        Component.update()
        expect(Component.find("button#testbutton")).toMatchSnapshot();


    });
});
