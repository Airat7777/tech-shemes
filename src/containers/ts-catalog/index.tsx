import React from "react";
import MetaTags from "react-meta-tags";
import styles from "./styles.css";
import { TableComponent } from "../../components/table";

import Button from "@material-ui/core/Button";

import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import CheckIcon from "@material-ui/icons/Check";
import UndoIcon from "@material-ui/icons/Undo";
import { NavBar } from "../nav-bar";
import { useSchemesList } from "../../hooks/useSchemesList";

const TsCatalog = () => {
  const state = useSchemesList();

  if (state.isLoading) {
    return <div>loading...</div>;
  }

  return (
    <>
      <MetaTags>
        <title>Список схем</title>
      </MetaTags>
      <NavBar/>
      <main className={styles.wrapper}>
        <h2 />
        <Button
          variant="contained"
          color="primary"
          size="small"
          startIcon={<AddIcon />}
          id="testbutton"
        >
          Добавить
        </Button>
        <Button
          variant="contained"
          color="primary"
          size="small"
          startIcon={<EditIcon />}
        >
          Редактировать
        </Button>
        <Button
          variant="contained"
          color="primary"
          size="small"
          startIcon={<DeleteIcon />}
        >
          Удалить
        </Button>
        <Button
          variant="contained"
          color="primary"
          size="small"
          startIcon={<CheckIcon />}
        >
          Применить
        </Button>
        <Button
          variant="contained"
          color="primary"
          size="small"
          startIcon={<UndoIcon />}
        >
          Отменить
        </Button>
        <h2 />
        <div>
          <TableComponent state={state}></TableComponent>
        </div>
      </main>
    </>
  );
};
export default TsCatalog;
