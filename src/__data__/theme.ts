import {createMuiTheme} from "@material-ui/core";
import {indigo} from "@material-ui/core/colors";


export const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#ffffff',
        },
        secondary: indigo,
    },
});
