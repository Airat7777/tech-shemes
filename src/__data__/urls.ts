import { getNavigations } from '@ijl/cli';
// import { getNavigations, getNavigationsValue } from '@ijl/cli';
// console.log(getNavigationsValue('tech-schemes'))

const navigations = getNavigations('tech-schemes');

export const baseUrl = navigations['tech-schemes'];

export const URLs = {
    viewing: {
        url: navigations['link.tech-schemes.viewing'],
    },
    catalog: {
        url: navigations['link.tech-schemes.catalog'],
    },
    editor: {
        url: navigations['link.tech-schemes.editor']
    }
}
// console.log(URLs);
