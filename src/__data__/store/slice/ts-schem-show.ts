import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    data: [],
    info: null,
    error: false,
    loading: false
}

const slice = createSlice({
    name: 'ts-schem-show',
    initialState,
    reducers: {
        handleSuccess(state, action: any) {
            state.data = action.payload;
            state.loading = false;
            state.error = false;
        },
        handleError(state, action: any) {
            state.error = action.payload;
            state.loading = false;
        },
        handleGet(state) {
            state.loading = true;
        }
    }
})

export const {handleSuccess, handleError, handleGet} = slice.actions;
export const reducer = slice.reducer;
