import { applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';
import { createStore as createStoreTK } from '@reduxjs/toolkit';

import combineReducer from './reducers';

const reducer = combineReducers({
    ['tech-schemes']: combineReducer
})

const composeEnhancers = composeWithDevTools({
    // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});

export const store = createStoreTK(reducer, /* preloadedState, */ composeEnhancers(
    applyMiddleware(thunk),
    // other store enhancers if any
));
