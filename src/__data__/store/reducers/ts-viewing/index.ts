import * as types from '../../../action-types';

const initialState = {
    data: [],
    info: null,
    error: false,
    loading: false
}

const handleGet = (state) => ({
    ...state,
    loading: true
});

const handleSuccess = (state, action) => ({
    ...initialState,
    data: action.data,
    loading: false,
    error: false
});

const handleError = (state, action) => ({
    ...state,
    loading: false,
    error: action.error,
});

const handlers = {
    [types.GET_SHEME.GET]: handleGet,
    [types.GET_SHEME.SUCCESS]: handleSuccess,
    [types.GET_SHEME.ERROR]: handleError
    //[types.GET_SHEME.NO_ACCESS]: ,
    //[types.GET_SHEME.WARNING]: ,
}

export default function (state = initialState, action) {
    return handlers[action.type] ? handlers[action.type](state, action) : state;
}
