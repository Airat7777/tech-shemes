import { combineReducers } from 'redux';
import { reducer as tsViewing } from '../slice/ts-viewing'
import { reducer as tsSchemeViewing } from '../slice/ts-schem-show'

export default combineReducers({
    tsViewing,
    tsSchemeViewing
});
