import { getConfigValue } from '@ijl/cli';
import { handleGet, handleError, handleSuccess } from '../store/slice/ts-viewing'
import axios from "axios";

export const getSchemesList = () => async (dispatch) => {
    dispatch(handleGet());
    // console.log('getSchemesList');
    const baseApiUrl = getConfigValue('tech-schemes.api');

    const getSchemesUrl = baseApiUrl + '/schemes-list/get-schemes-list';
    const response = await axios(getSchemesUrl, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
        }
    });

    if (response.data) {
        try {
            const answer = response.data;
            // console.log(answer);
            dispatch(handleSuccess(answer));
        } catch (error) {
            console.error(error.message);
            dispatch(handleError(error.message));
        }
    } else {
        try {
            const answer = response.data;

            const error = answer.error || 'Неизвестная ошибка'

            dispatch(handleError(error))
        } catch (error) {
            console.error(error.message);
        }
    }
}
