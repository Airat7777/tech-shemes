import { getConfigValue } from '@ijl/cli';
import { handleGet, handleError, handleSuccess } from '../store/slice/ts-viewing'

export const getSchemeById = () => async (dispatch) => {
    dispatch(handleGet());
    // console.log('getSchemeById');
    const baseApiUrl = getConfigValue('tech-schemes.api');

    const getSchemesUrl = baseApiUrl + '/schemes/0';
    const response = await fetch(getSchemesUrl, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
        }
    });

    if (response.ok) {
        try {
            const answer = await response.json();
            // console.log(answer);
            dispatch(handleSuccess(answer));
        } catch (error) {
            console.error(error.message);
            dispatch(handleError(error.message));
        }
    } else {
        try {
            const answer = await response.json();

            const error = answer.error || 'Неизвестная ошибка'

            dispatch(handleError(error))
        } catch (error) {
            console.error(error.message);
        }
    }
}
