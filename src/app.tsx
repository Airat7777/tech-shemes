import React from 'react';
import {BrowserRouter} from 'react-router-dom'
import Dashboard from './containers/dashboard'
import './app.css'
import { baseUrl } from "./__data__/urls";
import { store } from "./__data__/store";
import { Provider } from "react-redux";
import {MuiThemeProvider} from '@material-ui/core/styles';
import {theme} from "./__data__/theme";


const App = () => (
    <Provider store={store}>
        <MuiThemeProvider theme={theme}>
            <BrowserRouter basename={baseUrl}>
                <Dashboard/>
            </BrowserRouter>
        </MuiThemeProvider>
    </Provider>
)

export default App
