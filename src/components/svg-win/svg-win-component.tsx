import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import makeStyles from '@material-ui/core/styles/makeStyles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import React, {useState} from 'react';
import Dialog from '@material-ui/core/Dialog';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    rightBut: {
        marginRight: theme.spacing(0)
    },
    heroButtons: {
        height: '50%',
        marginTop: theme.spacing(4),
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
}));

export default function SvgSave() {
    const title = 'Котельная №1';
    const folder = 'Котельные';
    const classes = useStyles();
    const [open, setOpen] = useState(true);
    const [openGroup] = useState(false);

    // const handleCloseGroups = () => {
    //     setGroupOpen(false);
    // };

    const handleClose = () => {
        setOpen(false);
    };

    const save = () => {
        setOpen(false);
    };

    return (
        <React.Fragment>
            <Dialog open={open} fullWidth>
                <Container>
                    <form>
                        <Typography component="h6">
                            Наименование схемы
                        </Typography>
                        <TextField
                            margin="normal"
                            required
                            id="name"
                            label="Наименование"
                            value={title}
                            name="name"
                        />
                        <Typography component="h6">
                            Группа/Папка
                        </Typography>
                        <TextField
                            margin="normal"
                            required
                            id="folder"
                            label="Выберите группу"
                            value={folder}
                            name="folder"
                        />
                        <Button size="small">
                            ...
                        </Button>
                        <div>
                            <FormControlLabel
                                control={<Checkbox value="active"/>}
                                label="Активна"
                            />
                        </div>
                        <div className={classes.heroButtons}>
                            <Button size="small">
                                SVG
                            </Button>
                            <Button size="small" className={classes.rightBut}>
                                Выбрать SVG файл
                            </Button>
                            <Button size="small">
                                Предпросмотр
                            </Button>
                        </div>
                        {/*<CardMedia*/}
                        {/*    className={classes.cardMedia}*/}
                        {/*    image=""*/}
                        {/*    title="Вставьте SVG схему"*/}
                        {/*/>*/}
                        <div className={classes.heroButtons}>
                            <Grid container>
                                <Grid item>
                                    <Button variant="contained" onClick={handleClose}>
                                        CANCEL
                                    </Button>
                                </Grid>
                                <Grid item>
                                    <Button variant="contained" color="primary" onClick={save}>
                                        SAVE
                                    </Button>
                                </Grid>
                            </Grid>
                        </div>
                    </form>
                </Container>
            </Dialog>
            <Dialog open={openGroup} fullWidth>
                List of groups
            </Dialog>
        </React.Fragment>
    )
}
// }
