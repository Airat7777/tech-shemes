import React from 'react'
import {describe, it, expect} from '@jest/globals'
import {mount} from 'enzyme'
import SvgSave from "../../svg-win/svg-win-component";

describe('тестируем компонент SvgSave', () => {
    it('тестируем рендер компонента SvgSave', () => {
        const Component = mount(<SvgSave/>);
        expect(Component).toMatchSnapshot()
    });
});
