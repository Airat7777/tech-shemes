import React, {forwardRef, useImperativeHandle, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
        width: "100%",
        height: "100%",
    },
    img: {
        width: "100%",
        height: "100%",
        padding: "30px",
    }
});

export  const  SchemeViewer = (({imgSrc = ''}, ref) => {
    const classes = useStyles();
    const [scheme, setScheme] = useState(imgSrc);


    useImperativeHandle(ref, () => {
        return {
            setSchemeFromParent: (scheme) => {
                setScheme(scheme);
            }
        }
    });
    return (
        <div className={classes.root}>
            <img className={classes.img} src={scheme}/>
        </div>
    );
});

export default forwardRef(SchemeViewer);
