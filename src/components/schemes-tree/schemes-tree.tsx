import React from 'react';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import {useStylePanel} from "../../hooks/useStylePanel";
import {renderTree} from "../schemes-tree-editor/schemes-tree-editor";


// const TreeItem = withStyles({
//     root: {
//         "&.Mui-selected > .MuiTreeItem-content": {
//             color: "blue"
//         }
//     }
// })(MuiTreeItem);

export  const  SchemesTree = (({state, schemSelected}) => {
    const classes = useStylePanel();
    const [selected] = React.useState([]);
    // const modal = useRef(null);

    const handleSelect = (event, nodeIds) => {
        schemSelected(nodeIds);
    };

    // const renderTree = (nodes) => (
    //     (nodes && <TreeItem key={nodes.id} nodeId={nodes.id} label={nodes.name}>
    //         {Array.isArray(nodes.children) ? nodes.children.map((node) => renderTree(node)) : null}
    //     </TreeItem>)
    // );

    return (
        <div className={classes.box}>
            <h2 className={classes.header}>Список схем</h2>
            <div id="customId">
                <TreeView
                    className={classes.tree}
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpanded={['root']}
                    defaultExpandIcon={<ChevronRightIcon />}
                    selected={selected}
                    onNodeSelect={handleSelect}
                >
                    {state && state.data && state.data.schemesList && state.data.schemesList.map (item => renderTree(item))}

                </TreeView>
            </div>
        </div>
    );
});
