import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import makeStyles from '@material-ui/core/styles/makeStyles';
import React, {forwardRef, useImperativeHandle, useState} from 'react';
import Dialog from '@material-ui/core/Dialog';
import Container from '@material-ui/core/Container';
import {FormControl, InputLabel, NativeSelect} from "@material-ui/core";
import { For } from 'react-loops';

const useStyles = makeStyles((theme) => ({
    rightBut: {
        marginRight: theme.spacing(0)
    },
    heroButtons: {
        height: '50%',
        marginTop: theme.spacing(4),
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    itemsContainer: {
        display: 'grid',
        gridTemplateColumns: "50% 50%"
    }/*,
    item: {
        display: 'grid',
        gridTemplateRows: "50% 50%"
    }*/,
    container: {
        maxHeight: "400px",
        overflow: "scroll"
    },
    idsLabelCont: {
        display: "grid",
        gridTemplateRows: "30% 70%"
    },
    idsLabel: {
        fontSize: "11px"
    }
}));

export function SvgIdsMatchWindows({defaultOpened = false, idsList = [], tagsList = []}, ref, ) {
    const classes = useStyles();
    const [open, setOpen] = useState(defaultOpened)

    useImperativeHandle(ref, () => ({
        open: () => setOpen(true),
        close: () => setOpen(false)
    }), [close]);

    const handleClose = () => {
        setOpen(false);
    };

    const save = () => {
        setOpen(false);
    };
    // const handleIds = (event) => {
    //     console.log(event.target.value);
    // }

    const handleTags = (event) => {
        console.log(event.target.value);
    }

    return (
        <>
            <Dialog open={open} fullWidth>
                <Container className={classes.container}>
                    <For of={idsList} as={item =>
                        <div className={classes.itemsContainer}>
                            <div className={classes.idsLabelCont}>
                                <label className={classes.idsLabel}>id элемента</label>
                                {item}
                            </div>
                            <FormControl className={classes.formControl}>
                                <div>
                                    <InputLabel>Тег</InputLabel>
                                    <NativeSelect
                                        onChange={handleTags}
                                    >
                                        <For of={tagsList} as={item =>
                                            <option value={item}>{item}</option>
                                        }/>
                                    </NativeSelect>
                                </div>
                            </FormControl>
                        </div>
                    }/>
                    <Grid container>
                        <Grid item>
                            <Button variant="contained" onClick={handleClose}>
                                Отмена
                            </Button>
                        </Grid>
                        <Grid item>
                            <Button variant="contained" color="primary" onClick={save}>
                                Сохранить сопоставление
                            </Button>
                        </Grid>
                    </Grid>
                </Container>
            </Dialog>
        </>
    )
}

export default forwardRef(SvgIdsMatchWindows);
