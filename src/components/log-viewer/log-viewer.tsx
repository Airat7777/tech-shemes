import React, {useEffect, useState} from 'react';
import {useStylePanel} from "../../hooks/useStylePanel";

let logMessage = "";

export  const  LogViewer = ((props) => {
    const [setText] = useState(props.text);
    const classes = useStylePanel();

    useEffect(() => {
        // console.log('did mount');
        const interval = setInterval(() => {
            // console.log('interval tick');
            logMessage = format("Test message") + logMessage;
            setText(logMessage);
        }, 3000);
        console.log(interval);
    }, []);

    const format  = (text) => {
        return new Date().toLocaleTimeString() + ": " + text + "\n";
    }

    return (
        <div className={classes.box}>
            <h2 className={classes.header}>Лог сообщений</h2>
            <textarea readOnly={true} className={classes.content} value={logMessage} />
        </div>
    );
});
