import React from 'react'
import {describe, it, expect} from '@jest/globals'
import {mount} from 'enzyme'
import {LogViewer} from "../../log-viewer";

describe('тестируем компонент LogViewer', () => {
    it('тестируем рендер компонента LogViewer', () => {
        const Component = mount(<LogViewer/>);
        expect(Component).toMatchSnapshot()
    });
});
