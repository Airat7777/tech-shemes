import OriginalMaterialTable from 'material-table';
import React, { forwardRef } from 'react';

import OriginalAddBox from '@material-ui/icons/AddBox';
import OriginalArrowDownward from '@material-ui/icons/ArrowDownward';
import OriginalCheck from '@material-ui/icons/Check';
import OriginalChevronLeft from '@material-ui/icons/ChevronLeft';
import OriginalChevronRight from '@material-ui/icons/ChevronRight';
import OriginalClear from '@material-ui/icons/Clear';
import OriginalDeleteOutline from '@material-ui/icons/DeleteOutline';
import OriginalEdit from '@material-ui/icons/Edit';
import OriginalFilterList from '@material-ui/icons/FilterList';
import OriginalFirstPage from '@material-ui/icons/FirstPage';
import OriginalLastPage from '@material-ui/icons/LastPage';
import OriginalRemove from '@material-ui/icons/Remove';
import OriginalSaveAlt from '@material-ui/icons/SaveAlt';
import OriginalSearch from '@material-ui/icons/Search';
import OriginalViewColumn from '@material-ui/icons/ViewColumn';

//TODO добавить типизацию
const MaterialTable: any = OriginalMaterialTable;

const AddBox: any =  OriginalAddBox;
const ArrowDownward: any =  OriginalArrowDownward;
const Check: any =  OriginalCheck;
const ChevronLeft: any =  OriginalChevronLeft;
const ChevronRight: any =  OriginalChevronRight;
const Clear: any =  OriginalClear;
const DeleteOutline: any =  OriginalDeleteOutline;
const Edit: any =  OriginalEdit;
const FilterList: any =  OriginalFilterList;
const FirstPage: any =  OriginalFirstPage;
const LastPage: any =  OriginalLastPage;
const Remove: any =  OriginalRemove;
const SaveAlt: any =  OriginalSaveAlt;
const Search: any =  OriginalSearch;
const ViewColumn: any =  OriginalViewColumn;

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
    
    };
export const TableComponent = ({state}) => {

  const s = {schemesList: []}
  
  /* Parse data and fill new array */
  const newData = []
  if(state && state.data && state.data.schemesList){
    s.schemesList = state.data.schemesList;
    
    for(let i = 0; i < Object.keys(s.schemesList).length; i++)
    {
        newData.push({
              id: s.schemesList[i].id,
              name: s.schemesList[i].name,
              createDate: s.schemesList[i].createDate,
              user: s.schemesList[i].user,
              tags: s.schemesList[i].tags,
              status: s.schemesList[i].status,
              parentId: 0,
              children: []  
        });
        const ch = s.schemesList[i].children
        if(Object.keys(ch).length)
        for(let j = 0; j < ch.length; j++){
            newData.push({
              id: ch[j].id,
              name: ch[j].name,
              createDate: ch[j].createDate,
              user: ch[j].user,
              tags: ch[j].tags,
              status: ch[j].status,
              parentId: s.schemesList[j].id,
              children: []  
            });
        }
    }
  }
  
    return  (
        <MaterialTable

        icons={tableIcons}

      title="Технологические схемы"
      data = {newData}

      columns={[
        { title: 'Наименование схемы/группы', field: 'name', type: 'string' },
        { title: 'Дата создания', field: 'createDate', type: 'string' },
        { title: 'Загрузил', field: 'user', type: 'string' },
        { title: 'Привязок', field: 'tags', type: 'numeric' },
        { title: 'Активна', field: 'status', type: 'boolean' },
        
      ]}
      parentChildData={(row, rows) => rows.find(a => a.id === row.parentId)}

      options={{
        selection: true,
        search: false,
        headerStyle: {
          color: '#0000EE'
        }        
      }}

    />
    )
}