import React from 'react'
import {describe, it, expect} from '@jest/globals'
import {mount} from 'enzyme'
import Error from '../'

describe('тестируем компонент Error', () => {
    it('тестируем рендер компонента Error', () => {
        const Component = mount(<Error/>);
        expect(Component).toMatchSnapshot()
    });
    it('тестируем рендер компонента Error с переданной ошибкой', () => {
        const error = {
            title: 'Заголовок ошибки',
            text: 'Что-то пошло не так'
        }
        const Component = mount(<Error error={error}/>);
        expect(Component).toMatchSnapshot()
    });
});
