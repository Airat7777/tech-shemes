import React from 'react';
import {describe, it, expect} from '@jest/globals'
import {mount} from 'enzyme'
import ErrorBoundary from '../../error-boundary';
import LazyComponent from '../../lazy-component';

describe('тестируем компонент LazyComponent', () => {
    it('тестируем рендер компонента LazyComponent', () => {

        const children = React.createElement(ErrorBoundary)
        const fallback = null

        const Component = mount(<LazyComponent fallback={fallback}>
                {children}
        </LazyComponent>);
        expect(Component).toMatchSnapshot()
    });
});
