import React, {useRef} from 'react';
import {withStyles} from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import MuiTreeItem from "@material-ui/lab/TreeItem";
import {useStylePanel} from "../../hooks/useStylePanel";
import SvgIdsMatchWindows from "../svg-ids-match-windows/svg-ids-match-windows";



const TreeItem = withStyles({
    root: {
        "&.Mui-selected > .MuiTreeItem-content": {
            color: "blue"
        }
    }
})(MuiTreeItem);


export const renderTree = (nodes) => (
    (nodes && <TreeItem key={nodes.id} nodeId={nodes.id.toString()} label={nodes.name}>
        {Array.isArray(nodes.children) ? nodes.children.map((node) => renderTree(node)) : null}
    </TreeItem>)
);

export  const  SchemesTreeEditor = (({state, schemSelected}) => {
    const classes = useStylePanel();
    const [selected, setSelected] = React.useState([]);
    const [isOpen, setIsOpen]= React.useState(state);
    const modal = useRef(null);
    const idsList = [
        'svg-id1-well',
        'svg-id2-well',
        'svg-id3-well',
        'svg-id4-well',
        'svg-id5-well',
    ];

    const tagsList = [
        'TAG1.ДОБЫЧАПАРА',
        'TAG1.ОБВОДНЕННОСТЬ',
        'TAG1.ДЕБИТНЕФТИ',
        'TAG1.ДАВЛЕНИЕПАРА',
    ];
    const handleSelect = (event, nodeIds) => {
        // console.log('selected');
        setSelected(nodeIds);
        setIsOpen(true);
        schemSelected(nodeIds);
        setTimeout(()=> {
            modal.current.open();
        }, 1000);
    };

    return (
        <div className={classes.box}>
            <h2 className={classes.header}>Список схем</h2>
            <div id="customId">
                <SvgIdsMatchWindows ref={modal} idsList={idsList} tagsList={tagsList} />
                <TreeView
                    className={classes.tree}
                    defaultCollapseIcon={<ExpandMoreIcon />}
                    defaultExpanded={['root']}
                    defaultExpandIcon={<ChevronRightIcon />}
                    selected={selected}
                    onNodeSelect={handleSelect}
                >
                    {state && state.data && state.data.schemesList && state.data.schemesList.map (item => renderTree(item))}

                </TreeView>
            </div>
        </div>
    );
});
