const router = require("express").Router();

router.use(require('./schemes-list'));
router.use(require('./scheme-by-id'));

module.exports = router;
