const router = require("express").Router();

router.get('/scheme/:id', (req, res) => {
    setTimeout(() => {
        // console.log(req.id);
        res.send(require('./mocks/scheme/get-scheme-by-id.json'));
    }, 5000);
});

module.exports = router;
