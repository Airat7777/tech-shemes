const router = require("express").Router();

router.get('/schemes-list/get-schemes-list', (req, res) => {
    setTimeout(() => {
        res.send(require('./mocks/schemes-list/get-schemes-list.success.json'));
    }, 5000);
});

router.put('/schemes-list/add-schemes-list', (req, res) => {
    // console.log(req.body);
    res.send(require('./mocks/schemes-list-add/add-schemes-list.success.json'));
});

router.get('schemes-list/delete-schemes-list', (req, res) => {
    res.send(require('./mocks/schemes-list-delete/delete-schemes-list.success.json'));
});

router.get('/schemes-list/edit-schemes-list', (req, res) => {
    res.send(require('./mocks/schemes-list-edit/edit-schemes-list.success.json'));
});

module.exports = router;
