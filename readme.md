# Technological-schemes (TS)

## MVP1
- TS viewing page
- TS editor page  (for bind the id`s of the elements of the scheme)
- TS catalogue (create) page

## Install

npm install

## Run&Build

npm start
npm run-script build:prod

#### Project links
- [git (bitbacket) repository](https://bitbucket.org/Airat7777/tech-shemes/src)
- [jira](https://tech-schems.atlassian.net/secure/RapidBoard.jspa?projectKey=TSCH&rapidView=1)
- [figma](https://www.figma.com/file/Mok8vjTjsUuoodxPqIvI5Q/%D0%A2%D0%B5%D1%85%D0%BD%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B5-%D1%81%D1%85%D0%B5%D0%BC%D1%8B?node-id=6%3A47)

